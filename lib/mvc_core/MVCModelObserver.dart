abstract class MVCModelObserver {
  void update({int flag});
}
