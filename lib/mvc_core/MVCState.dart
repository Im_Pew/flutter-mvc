import 'package:flutter/widgets.dart';

import '../mvc.dart';

abstract class MVCState<V extends MVCStatefulView, M extends MVCModel> extends State<V>
    with MVCModelObserver {
  M model;

  MVCState(M model) {
    model.subscribe(this);
    this.model = model;
  }
}
