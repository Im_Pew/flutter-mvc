import 'package:flutter/widgets.dart';

class MVCEvent {
  MVCEvent(this._sender, this._value);

  Widget _sender;
  dynamic _value;

  Widget getSender() {
    return _sender;
  }

  dynamic getValue() {
    return _value;
  }
}
