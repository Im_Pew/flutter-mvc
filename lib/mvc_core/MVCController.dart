import 'dart:async';

import 'package:flutter/widgets.dart';

import '../mvc.dart';
import 'MVCModel.dart';

// ignore: must_be_immutable
abstract class MVCController<V extends MVCView, M extends MVCModel>
    extends StatefulWidget {
  // ignore: close_sinks
  StreamController<MVCEvent> _streamController;
  V view;
  M model;
  BuildContext context;

  void create(MVCView view, MVCModel model) {
    _streamController = StreamController();
    view.addNotifier(_streamController);

    _streamController.stream.listen(eventHandler);
  }

  void init() {
    _streamController = StreamController();
    view.addNotifier(_streamController);

    _streamController.stream.listen(eventHandler);
  }

  void eventHandler(MVCEvent event);
}

abstract class MVCControllerState<ExtendedController extends MVCController>
    extends State<ExtendedController> {
  @override
  Widget build(BuildContext context) {
    return widget.view.viewWidget;
  }

  @override
  void initState() {
    super.initState();

    widget.context = context;
  }

  @override
  void didUpdateWidget(ExtendedController oldWidget) {
    super.didUpdateWidget(oldWidget);

    widget.context = oldWidget.context;
  }

  @override
  void dispose() {
    widget._streamController.close();
    widget.context = null;

    super.dispose();
  }
}
