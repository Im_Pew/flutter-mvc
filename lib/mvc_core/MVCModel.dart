import '../mvc.dart';

abstract class MVCModel {
  var observers = [];

  void subscribe(MVCModelObserver observer) {
    observers.add(observer);
  }

  void unsubscribe(MVCModelObserver observer) {
    observers.remove(observer);
  }

  void notify({int flag}) {
    observers.forEach((element) {
      (element as MVCModelObserver).update(flag: flag);
    });
  }
}
