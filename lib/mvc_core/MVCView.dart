import 'dart:async';

import 'package:flutter/widgets.dart';

import '../mvc.dart';

abstract class MVCView {
  Widget viewWidget;
  StreamController controllerNotifier;

  void addNotifier(StreamController notifier) {
    this.controllerNotifier = notifier;
  }

  void notifyController(MVCEvent event) {
    controllerNotifier.add(event);
  }
}

// ignore: must_be_immutable
abstract class MVCStatefulView<M extends MVCModel> extends StatefulWidget with MVCView {
  M model;

  MVCStatefulView(M model) {
    this.viewWidget = this;
    this.model = model;
  }
}

// ignore: must_be_immutable
abstract class MVCStatelessView<M extends MVCModel> extends StatelessWidget
    with MVCView {
  M model;

  MVCStatelessView(M model) {
    this.viewWidget = this;
    this.model = model;
  }
}
